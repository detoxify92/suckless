# suckless builds

[suckless](https://suckless.org/) software simplifies the use of the computer
by providing simple, clear and hackable software. It is written in C and it is
designed to have a small amount of LOC to allow easy modification. In their
webpage there are plenty of patches that you can use to modify the behaviour
and appearance of the software with ease. I use this software together with my
[dotfiles](https://gitlab.com/detoxify92/dotfiles) to adjust the systems I use
to my liking.  My builds include:

- [dwm](https://suckless.org/). The dynamic window manager, is a tiling window
  manager for X11. I have included patches for having a systray and useless
  gaps in one of the two builds I include. The gaps are nice but in my laptop
  which has a small screen, all pixels are needed.

- [dmenu](https://tools.suckless.org/dmenu/). Is a dynamic menu used by dwm,
  but also very handy for user defined menus. Many of the scripts of my
  dotfiles use it.

- [dwmblocks](https://github.com/torrinfail/dwmblocks). The dwm bar can be
  updated just by modyfing the root window's name, but this tool allows to
  implement a modular status bar. Simply write modules that outputs the
  information you want for your bar and include them here. You will need the
  modules from my [dotfiles](https://gitlab.com/detoxify92/dotfiles) for this
  to work

- [slock](https://tools.suckless.org/slock/). A simple screen lock I use for
  the laptop. Simply locks the screen and requires password to enter. I use the
  build from [khuedoan](https://github.com/khuedoan/slock) which allows
  blurring if you are using picom.

## Installation
Clone the repository and build the software you want using

```sh
make
sudo make install
```

How to start dwm as your window manager will depend on the login mechanism you
use on your machine. If you use a desktop environment like GNOME or Plasma,
most likely you will use a display manager. Check
[this](https://wiki.archlinux.org/title/Display_manager#Session_configuration)
to add an entry for dwm. If you use xinit, just add 

```sh
exec dwm
```

at the end of your xinitrc, see [here](https://wiki.archlinux.org/title/Xinit).
