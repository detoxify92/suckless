#!/bin/sh

# Script to change colors in dwm, dmenu and more

## COLORS
# Ubuntu colors
#col_fg=#e95420
#col_col=#ee8d66
#col_bg=#2b2929

# Gruvbox
#col_fg=#ebdbb2
#col_bg=#282828
#col_col=#d79921
#col_col=#98971a
#col_col=#b16286
#col_col=#fb4934

# Gruvbox material
col_fg=#d4be98
col_bg=#282828
#col_col=#ea6962
#col_col=#e78a4e
col_col=#d8a657
#col_col=#a9b665
#col_col=#89b482
#col_col=#7daea3
#col_col=#d3869b

# Everforest
#col_fg=#d3c6aa
#col_bg=#2b3339
#col_col=#d699b6

## FONT 
size="9"
font="monospace:size=${size}"

## BAR HEIGHT
# Height of 0 means it is calculated by dwm/dmenu
height=0

## MIX PARAMETER FOR NOTIFICATION AND OPACITY
mix_p=0.9
opa=bb

## PARSING ARGUMENTS
r_dwm=1
r_dmenu=1
r_dunst=1
reverse_colors=1

## RICING
# dwm
if [ $r_dwm = 1 ]
then
   echo "Ricing dwm..."
   cd dwm
   if [ $reverse_colors = 1 ]
   then
      sed -i '/SchemeSel/ s/col_bg, col_col/col_col, col_bg/g' config.def.h
   fi
   sed -i "/col_col\[\]/ s/\".*\"/\"$col_col\"/" config.def.h
   sed -i "/col_fg\[\]/ s/\".*\"/\"$col_fg\"/" config.def.h
   sed -i "/col_bg\[\]/ s/\".*\"/\"$col_bg\"/" config.def.h
   sed -i "/fonts\[\]/ s/\".*\"/\"$font\"/" config.def.h
   sed -i "/font\[\]/ s/\".*\"/\"$font\"/" config.def.h
   sed -i "/int[[:space:]]user\_bh/ s/= [0-9]*;/= $height;/" config.def.h
   sed -i "/dmenucmd\[/ s/-h\",[[:space:]]\"[0-9]*\"/-h\", \"$height\"/" config.def.h
   rm config.h
   make
   sudo make install
fi

# dmenu
if [ $r_dmenu = 1 ]
then
   echo "Ricing dmenu..."
   cd ../dmenu
   sed -i "/col_col\[\]/ s/\".*\"/\"$col_col\"/" config.def.h
   sed -i "/col_fg\[\]/ s/\".*\"/\"$col_fg\"/" config.def.h
   sed -i "/col_bg\[\]/ s/\".*\"/\"$col_bg\"/" config.def.h
   sed -i "/fonts\[\]/ s/\".*\"/\"$font\"/" config.def.h
   sed -i "/int[[:space:]]lineheight/ s/= [0-9]*;/= $height;/" config.def.h
   rm config.h
   make
   sudo make install
fi

# dunst
if [ $r_dunst = 1 ]
then
   echo "Ricing dunst..."
   cd ~/.config/dunst
   red_d=$(printf "%x\n" $(printf "%s*%d + (1-%s)*%d\n" $mix_p 0x$(echo $col_bg | cut -b 2-3) $mix_p 0x$(echo $col_col | cut -b 2-3) | bc))
   green_d=$(printf "%x\n" $(printf "%s*%d + (1-%s)*%d\n" $mix_p 0x$(echo $col_bg | cut -b 4-5) $mix_p 0x$(echo $col_col | cut -b 4-5) | bc))
   blue_d=$(printf "%x\n" $(printf "%s*%d + (1-%s)*%d\n" $mix_p 0x$(echo $col_bg | cut -b 6-7) $mix_p 0x$(echo $col_col | cut -b 6-7) | bc))
   size_dunst=$(echo "$size - 1" | bc)
   sed -i --follow-symlinks "/background/ s/\".*\"/\"#$red_d$green_d$blue_d$opa\"/" dunstrc
   sed -i --follow-symlinks "/foreground/ s/\".*\"/\"$col_fg\"/" dunstrc
   sed -i --follow-symlinks "/monospace/ s/ [0-9]*$/ $size_dunst/" dunstrc
   killall dunst
   dunst &
fi
